import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:rick_and_morty_final/firebase_options.dart';
import 'package:rick_and_morty_final/locator_service.dart';
import 'package:rick_and_morty_final/presentation/bloc/cubit/auth_cubit.dart';
import 'package:rick_and_morty_final/presentation/bloc/rick_morty_bloc.dart';
import 'package:rick_and_morty_final/presentation/providers/filter_character_provider.dart';
import 'package:rick_and_morty_final/presentation/providers/filter_location_provider.dart';
import 'package:rick_and_morty_final/presentation/providers/rick_morty_provider.dart';
import 'package:rick_and_morty_final/presentation/router/app_router.dart';
import 'package:rick_and_morty_final/presentation/theme/app_theme.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  await init();
  runApp(
    ChangeNotifierProvider(
      create: (BuildContext context) => RickMortyProvider(),
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    final appRouter = AppRouter();
    return MultiBlocProvider(
      providers: [
        BlocProvider<RickMortyBloc>(create: (context) => sl<RickMortyBloc>()),
        BlocProvider<AuthCubit>(create: (context) => sl<AuthCubit>()),
        ChangeNotifierProvider(create: (context) => FilterLocationProvider()),
        ChangeNotifierProvider(create: (context) => FilterCharacterProvider()),
      ],
      child: MaterialApp.router(
        debugShowCheckedModeBanner: false,
        routerConfig: appRouter.config(),
        title: 'Rick and morty',
        theme: Provider.of<RickMortyProvider>(context, listen: true).isDarkTheme
            ? AppTheme.darkMode
            : AppTheme.lightMode,
      ),
    );
  }
}

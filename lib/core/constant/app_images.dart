// **************************************************************************
// * AssetsGenerator - Simpler FLutter Generator Extension -
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND


class AppImages {
  AppImages._();
  
  static const String avatar = 'assets/images/avatar.png';  
  static const String episodeBg = 'assets/images/episode_bg.png';  
  static const String logo = 'assets/images/logo.png';  
  static const String mortyLogo = 'assets/images/morty_logo.png';  
  static const String noCharacter = 'assets/images/no_character.png';  
  static const String noEpisode = 'assets/images/no_episode.png';  
  static const String noLocation = 'assets/images/no_location.png';  
  static const String rick = 'assets/images/rick.png';  
  static const String splashScreen = 'assets/images/splash_screen.png';  
  static const String splashScreenLight = 'assets/images/splash_screen_light.png';  
}

// **************************************************************************
// * AssetsGenerator - Simpler FLutter Generator Extension -
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND


class AppIcons {
  AppIcons._();
  
  static const String artist = 'assets/icons/artist.svg';  
  static const String ascending = 'assets/icons/ascending.svg';  
  static const String descending = 'assets/icons/descending.svg';  
  static const String episodes = 'assets/icons/episodes.svg';  
  static const String location = 'assets/icons/location.svg';  
  static const String password = 'assets/icons/password.png';  
  static const String settings = 'assets/icons/settings.svg';  
  static const String subtract = 'assets/icons/subtract.svg';  
  static const String user = 'assets/icons/user.png';  
}

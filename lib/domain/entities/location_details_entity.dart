import 'package:equatable/equatable.dart';

class LocationDetailsEntity extends Equatable {
  final int id;
  final String name;
  final String type;
  final String dimension;
  final List<String> residents;
  final String created;

  const LocationDetailsEntity({
    required this.id,
    required this.name,
    required this.type,
    required this.dimension,
    required this.residents,
    required this.created,
  });

  @override
  List<Object?> get props => [id, name, type, dimension, residents, created];
}

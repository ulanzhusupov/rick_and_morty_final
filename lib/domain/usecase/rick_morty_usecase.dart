import 'package:rick_and_morty_final/domain/entities/episode_entity.dart';
import 'package:rick_and_morty_final/domain/entities/location_details_entity.dart';
import 'package:rick_and_morty_final/domain/entities/person_entity.dart';
import 'package:rick_and_morty_final/domain/repositories/rick_morty_repository.dart';

class RickMortyUseCase {
  RickMortyUseCase({required this.rickMortyRepository});
  final RickMortyRepository rickMortyRepository;

  List<PersonEntity>? get characters => _characters;
  List<PersonEntity>? get queryCharacters => _queryCharacters;
  List<EpisodeEntity>? get episodes => _episodes;
  List<LocationDetailsEntity>? get locations => _locations;

  List<PersonEntity>? _characters;
  List<PersonEntity>? _queryCharacters;
  List<EpisodeEntity>? _episodes;
  List<LocationDetailsEntity>? _locations;

  Future<void> getAllCharacters(int page) async {
    final model = await rickMortyRepository.getAllCharacters(page);
    _characters = model;
  }

  Future<void> searchCharacter(String query) async {
    final model = await rickMortyRepository.searchCharacter(query);
    _characters = model;
  }

  Future<void> getSpecialCharacters(List<String> characters) async {
    final model = await rickMortyRepository.getSpecialCharacters(characters);
    _characters = model;
  }

  Future<void> searchEpisodes(String query) async {
    final model = await rickMortyRepository.searchEpisodes(query);
    _episodes = model;
  }

  Future<void> getAllEpisodes(int page) async {
    final model = await rickMortyRepository.getAllEpisodes(page);
    _episodes = model;
  }

  Future<void> getSpecialEpisodes(List<String> episodes) async {
    final model = await rickMortyRepository.getSpecialEpisodes(episodes);
    _episodes = model;
  }

  Future<void> getAllLocations(int page) async {
    final model = await rickMortyRepository.getAllLocations(page);
    _locations = model;
  }

  Future<void> searchLocation(String query) async {
    final model = await rickMortyRepository.searchLocations(query);
    _locations = model;
  }

  Future<void> filterLocation(String type, String dimension) async {
    final model = await rickMortyRepository.filterLocations(type, dimension);
    _locations = model;
  }
}

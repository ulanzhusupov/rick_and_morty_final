import 'package:rick_and_morty_final/domain/entities/episode_entity.dart';
import 'package:rick_and_morty_final/domain/entities/location_details_entity.dart';
import 'package:rick_and_morty_final/domain/entities/person_entity.dart';

abstract class RickMortyRepository {
  Future<List<PersonEntity>> getAllCharacters(int page);
  Future<List<PersonEntity>> searchCharacter(String query);
  Future<List<PersonEntity>> getSpecialCharacters(List<String> characters);
  Future<List<EpisodeEntity>> getAllEpisodes(int page);
  Future<List<EpisodeEntity>> getSpecialEpisodes(List<String> episodes);
  Future<List<EpisodeEntity>> searchEpisodes(String query);
  Future<List<LocationDetailsEntity>> getAllLocations(int page);
  Future<List<LocationDetailsEntity>> searchLocations(String query);
  Future<List<LocationDetailsEntity>> filterLocations(
    String type,
    String dimension,
  );
}

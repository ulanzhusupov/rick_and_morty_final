import 'package:get_it/get_it.dart';
import 'package:rick_and_morty_final/core/network/dio_settings.dart';
import 'package:rick_and_morty_final/data/data_source/rick_morty_remote_data_source.dart';
import 'package:rick_and_morty_final/data/repositories/rick_morty_repository_impl.dart';
import 'package:rick_and_morty_final/domain/repositories/rick_morty_repository.dart';
import 'package:rick_and_morty_final/domain/usecase/rick_morty_usecase.dart';
import 'package:rick_and_morty_final/presentation/bloc/cubit/auth_cubit.dart';
import 'package:rick_and_morty_final/presentation/bloc/rick_morty_bloc.dart';
import 'package:rick_and_morty_final/presentation/firebase_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

final sl = GetIt.instance;

Future<void> init() async {
  // BLOC-CUBIT
  sl.registerFactory(() => RickMortyBloc(rickMortyUseCase: sl()));
  sl.registerFactory(() => AuthCubit(authService: sl(), preferences: sl()));

  // Use-cases
  sl.registerLazySingleton(() => RickMortyUseCase(rickMortyRepository: sl()));

  // Repository
  sl.registerLazySingleton<RickMortyRepository>(
      () => RickMortyRepositoryImpl(remoteDataSource: sl()));

  // Data-Source
  sl.registerLazySingleton<RickMortyRemoteDataSource>(
      () => RickMortyRemoteDataSourceImpl(dioSettings: sl()));

  // External
  final sharedPref = await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPref);

  sl.registerLazySingleton(() => FirebaseAuthService());
  sl.registerLazySingleton(() => DioSettings());
}

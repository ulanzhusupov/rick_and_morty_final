import 'dart:developer';

import 'package:rick_and_morty_final/core/network/dio_settings.dart';
import 'package:rick_and_morty_final/data/models/episode_model.dart';
import 'package:rick_and_morty_final/data/models/location_details_model.dart';
import 'package:rick_and_morty_final/data/models/person_model.dart';
import 'package:rick_and_morty_final/domain/entities/episode_entity.dart';
import 'package:rick_and_morty_final/domain/entities/location_details_entity.dart';
import 'package:rick_and_morty_final/domain/entities/person_entity.dart';

abstract class RickMortyRemoteDataSource {
  Future<List<PersonEntity>> getAllCharacters(int page);
  Future<List<PersonEntity>> searchCharacter(String query);
  Future<List<PersonEntity>> getSpecialCharacters(List<String> characters);
  Future<List<EpisodeEntity>> getAllEpisodes(int page);
  Future<List<EpisodeEntity>> getSpecialEpisode(List<String> episodes);
  Future<List<EpisodeEntity>> searchEpisodes(String query);
  Future<List<LocationDetailsEntity>> getAllLocations(int page);
  Future<List<LocationDetailsEntity>> searchLocation(String query);
  Future<List<LocationDetailsEntity>> filterLocation(
      String type, String dimension);
}

class RickMortyRemoteDataSourceImpl extends RickMortyRemoteDataSource {
  RickMortyRemoteDataSourceImpl({required this.dioSettings});

  final DioSettings dioSettings;

  @override
  Future<List<PersonEntity>> getAllCharacters(int page) async {
    try {
      final result = await dioSettings.dio.get(
        "https://rickandmortyapi.com/api/character/?page=$page",
      );

      final data = result.data['results'];
      return (data as List).map((elem) => PersonModel.fromJson(elem)).toList();
    } catch (e) {
      log("Файл data -> RemoteDataSource: ${e.toString()}");
      throw UnimplementedError();
    }
  }

  @override
  Future<List<PersonEntity>> searchCharacter(String query) async {
    try {
      final result = await dioSettings.dio.get(
        "https://rickandmortyapi.com/api/character/?name=$query",
      );

      final data = result.data['results'];
      return (data as List).map((elem) => PersonModel.fromJson(elem)).toList();
    } catch (e) {
      log("Файл data -> RemoteDataSource: ${e.toString()}");
      throw UnimplementedError();
    }
  }

  @override
  Future<List<PersonEntity>> getSpecialCharacters(
      List<String> characters) async {
    try {
      String specialEpisodeUrl = "https://rickandmortyapi.com/api/character/";
      for (String episode in characters) {
        List<String> splitted = episode.split("/");
        specialEpisodeUrl += "${splitted[splitted.length - 1]},";
      }

      final result = await dioSettings.dio.get(specialEpisodeUrl);
      final data = result.data;

      return (data as List).map((e) => PersonModel.fromJson(e)).toList();
    } catch (e) {
      log("Файл data -> RemoteDataSource: ${e.toString()}");
      throw UnimplementedError();
    }
  }

  @override
  Future<List<EpisodeEntity>> getAllEpisodes(int page) async {
    try {
      final result = await dioSettings.dio.get(
        "https://rickandmortyapi.com/api/episode?page=$page",
      );

      final data = result.data['results'];
      return (data as List).map((elem) => EpisodeModel.fromJson(elem)).toList();
    } catch (e) {
      log("Файл data -> RemoteDataSource: ${e.toString()}");
      throw UnimplementedError();
    }
  }

  @override
  Future<List<EpisodeEntity>> getSpecialEpisode(List<String> episodes) async {
    try {
      String specialEpisodeUrl = "https://rickandmortyapi.com/api/episode/";
      for (String episode in episodes) {
        List<String> splitted = episode.split("/");
        specialEpisodeUrl += "${splitted[splitted.length - 1]},";
      }

      final result = await dioSettings.dio.get(specialEpisodeUrl);
      final data = result.data;

      return (data as List).map((e) => EpisodeModel.fromJson(e)).toList();
    } catch (e) {
      log("Файл data -> RemoteDataSource: ${e.toString()}");
      throw UnimplementedError();
    }
  }

  @override
  Future<List<EpisodeEntity>> searchEpisodes(String query) async {
    try {
      final result = await dioSettings.dio.get(
        "https://rickandmortyapi.com/api/episode/?name=$query",
      );

      final data = result.data['results'];
      return (data as List).map((elem) => EpisodeModel.fromJson(elem)).toList();
    } catch (e) {
      log("Файл data -> RemoteDataSource: ${e.toString()}");
      throw UnimplementedError();
    }
  }

  @override
  Future<List<LocationDetailsEntity>> getAllLocations(int page) async {
    try {
      final result = await dioSettings.dio.get(
        "https://rickandmortyapi.com/api/location?page=$page",
      );

      final data = result.data['results'];
      return (data as List)
          .map((elem) => LocationDetailsModel.fromJson(elem))
          .toList();
    } catch (e) {
      log("Файл data -> RemoteDataSource: ${e.toString()}");
      throw UnimplementedError();
    }
  }

  @override
  Future<List<LocationDetailsEntity>> searchLocation(String query) async {
    try {
      final result = await dioSettings.dio.get(
        "https://rickandmortyapi.com/api/location/?name=$query",
      );

      final data = result.data['results'];
      return (data as List)
          .map((elem) => LocationDetailsModel.fromJson(elem))
          .toList();
    } catch (e) {
      log("Файл data -> RemoteDataSource: ${e.toString()}");
      throw UnimplementedError();
    }
  }

  @override
  Future<List<LocationDetailsEntity>> filterLocation(
      String type, String dimension) async {
    try {
      final result = await dioSettings.dio.get(
        "https://rickandmortyapi.com/api/location/?type=$type&dimension=$dimension",
      );

      final data = result.data['results'];
      return (data as List)
          .map((elem) => LocationDetailsModel.fromJson(elem))
          .toList();
    } catch (e) {
      log("Файл data -> RemoteDataSource: ${e.toString()}");
      throw UnimplementedError();
    }
  }
}

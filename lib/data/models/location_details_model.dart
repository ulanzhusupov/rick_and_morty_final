import 'package:rick_and_morty_final/domain/entities/location_details_entity.dart';

class LocationDetailsModel extends LocationDetailsEntity {
  const LocationDetailsModel(
      {required super.id,
      required super.name,
      required super.type,
      required super.dimension,
      required super.residents,
      required super.created});

  factory LocationDetailsModel.fromJson(Map<String, dynamic> json) {
    return LocationDetailsModel(
        id: json['id'],
        name: json['name'],
        type: json['type'],
        dimension: json['dimension'],
        residents: json['residents'].cast<String>(),
        created: json['created']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['type'] = type;
    data['dimension'] = dimension;
    data['residents'] = residents;
    data['created'] = created;
    return data;
  }
}

import 'dart:developer';

import 'package:rick_and_morty_final/data/data_source/rick_morty_remote_data_source.dart';
import 'package:rick_and_morty_final/domain/entities/episode_entity.dart';
import 'package:rick_and_morty_final/domain/entities/location_details_entity.dart';
import 'package:rick_and_morty_final/domain/entities/person_entity.dart';
import 'package:rick_and_morty_final/domain/repositories/rick_morty_repository.dart';

class RickMortyRepositoryImpl extends RickMortyRepository {
  RickMortyRepositoryImpl({required this.remoteDataSource});

  final RickMortyRemoteDataSource remoteDataSource;

  @override
  Future<List<PersonEntity>> getAllCharacters(int page) async {
    try {
      return await remoteDataSource.getAllCharacters(page);
    } catch (e) {
      log("Файл data -> repositoy: ${e.toString()}");
      throw UnimplementedError();
    }
  }

  @override
  Future<List<PersonEntity>> searchCharacter(String query) async {
    try {
      return await remoteDataSource.searchCharacter(query);
    } catch (e) {
      log("Файл data -> repositoy: ${e.toString()}");
      throw UnimplementedError();
    }
  }

  @override
  Future<List<EpisodeEntity>> getAllEpisodes(int page) async {
    try {
      return await remoteDataSource.getAllEpisodes(page);
    } catch (e) {
      log("Файл data -> repositoy: ${e.toString()}");
      throw UnimplementedError();
    }
  }

  @override
  Future<List<EpisodeEntity>> getSpecialEpisodes(List<String> episodes) async {
    try {
      return await remoteDataSource.getSpecialEpisode(episodes);
    } catch (e) {
      log("Файл data -> repositoy: ${e.toString()}");
      throw UnimplementedError();
    }
  }

  @override
  Future<List<LocationDetailsEntity>> getAllLocations(int page) async {
    try {
      return await remoteDataSource.getAllLocations(page);
    } catch (e) {
      log("Файл data -> repositoy: ${e.toString()}");
      throw UnimplementedError();
    }
  }

  @override
  Future<List<EpisodeEntity>> searchEpisodes(String query) async {
    try {
      return await remoteDataSource.searchEpisodes(query);
    } catch (e) {
      log("Файл data -> repositoy: ${e.toString()}");
      throw UnimplementedError();
    }
  }

  @override
  Future<List<LocationDetailsEntity>> searchLocations(String query) async {
    try {
      return await remoteDataSource.searchLocation(query);
    } catch (e) {
      log("Файл data -> repositoy: ${e.toString()}");
      throw UnimplementedError();
    }
  }

  @override
  Future<List<PersonEntity>> getSpecialCharacters(
    List<String> characters,
  ) async {
    try {
      return await remoteDataSource.getSpecialCharacters(characters);
    } catch (e) {
      log("Файл data -> repositoy: ${e.toString()}");
      throw UnimplementedError();
    }
  }

  @override
  Future<List<LocationDetailsEntity>> filterLocations(
      String type, String dimension) async {
    try {
      return await remoteDataSource.filterLocation(type, dimension);
    } catch (e) {
      log("Файл data -> repositoy: ${e.toString()}");
      throw UnimplementedError();
    }
  }
}

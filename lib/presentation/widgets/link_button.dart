import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:rick_and_morty_final/presentation/providers/rick_morty_provider.dart';
import 'package:rick_and_morty_final/presentation/theme/app_colors.dart';
import 'package:rick_and_morty_final/presentation/theme/app_fonts.dart';

class LinkButton extends StatelessWidget {
  const LinkButton({
    super.key,
    this.icon,
    required this.firstText,
    required this.secondText,
    required this.onTap,
  });

  final String? icon;
  final String firstText;
  final String secondText;
  final Function() onTap;

  @override
  Widget build(BuildContext context) {
    final vm = context.watch<RickMortyProvider>();
    return InkWell(
      onTap: onTap,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          (icon != null)
              ? Row(
                  children: [
                    SvgPicture.asset(
                      icon ?? "",
                      color: vm.isDarkTheme ? Colors.white : AppColors.darkText,
                    ),
                    const SizedBox(width: 16),
                  ],
                )
              : const SizedBox(),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  firstText,
                  style: AppFonts.s16W400,
                ),
                Text(
                  secondText,
                  style: AppFonts.s14W400.copyWith(color: AppColors.grey),
                ),
              ],
            ),
          ),
          RotatedBox(
            quarterTurns: 10,
            child: IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.arrow_back_ios_new_outlined,
                color: Colors.white,
                size: 16,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

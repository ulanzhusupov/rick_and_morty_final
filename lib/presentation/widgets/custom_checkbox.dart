import 'package:flutter/material.dart';
import 'package:rick_and_morty_final/presentation/theme/app_fonts.dart';

class CustomCheckBox extends StatelessWidget {
  const CustomCheckBox({
    super.key,
    required this.label,
    required this.onTap,
    required this.value,
  });
  final bool value;
  final String label;
  final Function(bool? val) onTap;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SizedBox(
          width: MediaQuery.of(context).size.width * 0.8,
          child: CheckboxListTile(
            title: Text(
              label,
              style: AppFonts.s16W400.copyWith(color: Colors.white),
            ),
            controlAffinity: ListTileControlAffinity.leading,
            value: value,
            onChanged: onTap,
          ),
        ),
      ],
    );
  }
}

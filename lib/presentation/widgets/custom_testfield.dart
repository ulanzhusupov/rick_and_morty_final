import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rick_and_morty_final/presentation/providers/rick_morty_provider.dart';
import 'package:rick_and_morty_final/presentation/theme/app_colors.dart';
import 'package:rick_and_morty_final/presentation/theme/app_fonts.dart';

class CustomTextField extends StatelessWidget {
  const CustomTextField({
    super.key,
    required this.label,
    required this.hintText,
    required this.controller,
    this.icon,
    this.keyboardType,
  });

  final String label;
  final String hintText;
  final TextEditingController controller;
  final Icon? icon;
  final TextInputType? keyboardType;

  @override
  Widget build(BuildContext context) {
    final theme = context.watch<RickMortyProvider>();
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            label,
            style: AppFonts.s14W400,
          ),
          const SizedBox(height: 8),
          TextField(
            controller: controller,
            style: AppFonts.s16W400,
            keyboardType: keyboardType,
            decoration: InputDecoration(
              prefixIcon: icon,
              contentPadding: const EdgeInsets.symmetric(
                vertical: 12,
                horizontal: 16,
              ),
              hintText: hintText,
              hintStyle: AppFonts.s16W400.copyWith(color: AppColors.grey),
              border: OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: BorderRadius.circular(12),
              ),
              filled: true,
              fillColor:
                  theme.isDarkTheme ? AppColors.lightBg : AppColors.greyText,
            ),
          ),
        ],
      ),
    );
  }
}

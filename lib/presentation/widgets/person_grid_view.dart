import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rick_and_morty_final/presentation/providers/rick_morty_provider.dart';
import 'package:rick_and_morty_final/presentation/theme/app_colors.dart';
import 'package:rick_and_morty_final/presentation/theme/app_fonts.dart';

class PersonGridView extends StatelessWidget {
  const PersonGridView({
    super.key,
    required this.status,
    required this.fullName,
    required this.type,
    required this.gender,
    required this.image,
  });

  final String status;
  final String fullName;
  final String type;
  final String gender;
  final String image;

  @override
  Widget build(BuildContext context) {
    final theme = context.watch<RickMortyProvider>();
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: SizedBox(
              width: 100,
              height: 100,
              child: CircleAvatar(
                backgroundImage: NetworkImage(image),
              ),
            ),
          ),
          const SizedBox(height: 18),
          Column(
            children: [
              Text(
                status,
                style: AppFonts.s10W500.copyWith(
                  color: status == "Alive"
                      ? AppColors.green
                      : status == "unknown"
                          ? Colors.orange
                          : Colors.red,
                ),
              ),
              Text(
                fullName,
                textAlign: TextAlign.center,
                style: AppFonts.s16W500.copyWith(
                  color: theme.isDarkTheme ? Colors.white : AppColors.darkText,
                ),
              ),
              Text(
                "$type, $gender",
                style: AppFonts.s12W400.copyWith(
                  color: AppColors.grey,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

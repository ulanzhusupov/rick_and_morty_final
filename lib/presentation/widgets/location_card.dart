import 'package:flutter/material.dart';
import 'package:rick_and_morty_final/presentation/theme/app_colors.dart';
import 'package:rick_and_morty_final/presentation/theme/app_fonts.dart';

class LocationCard extends StatelessWidget {
  const LocationCard({
    super.key,
    required this.name,
    required this.type,
    required this.dimension,
    required this.isDark,
  });

  final String name;
  final String type;
  final String dimension;
  final bool isDark;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 12),
      child: Container(
        width: double.infinity,
        color: isDark
            ? AppColors.lightBg
            : const Color.fromARGB(255, 220, 248, 225),
        padding: const EdgeInsets.symmetric(
          vertical: 12,
          horizontal: 16,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.65,
                  child: Text(
                    name,
                    style: AppFonts.s20W500,
                  ),
                ),
                Text(
                  "$type · $dimension",
                  style: AppFonts.s12W400.copyWith(color: AppColors.grey),
                ),
              ],
            ),
            RotatedBox(
              quarterTurns: 10,
              child: IconButton(
                onPressed: () {},
                icon: Icon(
                  Icons.arrow_back_ios_new_outlined,
                  color: isDark ? Colors.white : AppColors.darkText,
                  size: 16,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

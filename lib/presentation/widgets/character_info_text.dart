import 'package:flutter/material.dart';
import 'package:rick_and_morty_final/presentation/theme/app_colors.dart';
import 'package:rick_and_morty_final/presentation/theme/app_fonts.dart';

class CharacterInfoText extends StatelessWidget {
  const CharacterInfoText({
    super.key,
    required this.title,
    required this.content,
  });
  final String title;
  final String content;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: AppFonts.s12W400.copyWith(color: AppColors.grey),
        ),
        Text(content, style: AppFonts.s14W400),
      ],
    );
  }
}

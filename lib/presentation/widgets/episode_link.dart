import 'package:flutter/material.dart';
import 'package:rick_and_morty_final/presentation/theme/app_colors.dart';
import 'package:rick_and_morty_final/presentation/theme/app_fonts.dart';

class EpisodeLink extends StatelessWidget {
  const EpisodeLink({
    super.key,
    required this.episode,
    required this.title,
    required this.date,
  });
  final String episode;
  final String title;
  final String date;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 12),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                episode,
                style: AppFonts.s10W500.copyWith(color: AppColors.grey),
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.6,
                child: Text(
                  title,
                  style: AppFonts.s16W500,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              Text(
                date,
                style: AppFonts.s14W400.copyWith(color: AppColors.grey),
              ),
            ],
          ),
          RotatedBox(
            quarterTurns: 10,
            child: IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.arrow_back_ios_new_outlined,
                color: Colors.white,
                size: 16,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

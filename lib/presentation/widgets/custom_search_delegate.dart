import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rick_and_morty_final/presentation/bloc/rick_morty_bloc.dart';
import 'package:rick_and_morty_final/presentation/widgets/person_row_view.dart';

class CustomSearchDelegate extends SearchDelegate {
  CustomSearchDelegate({required this.searchQuery})
      : super(searchFieldLabel: searchQuery);

  final String searchQuery;

  final _suggestions = [
    'Rick',
    'Morty',
    'Summer',
    'Beth',
    'Jerry',
  ];

  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
        onPressed: () {
          query = '';
          showSuggestions(context);
        },
        icon: const Icon(Icons.clear),
      ),
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(
      onPressed: () => close(context, null),
      icon: const Icon(Icons.arrow_back_outlined),
      tooltip: "Back",
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    BlocProvider.of<RickMortyBloc>(context, listen: false)
        .add(SearchCharacterEvent(query: query));

    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.9,
      child: BlocBuilder<RickMortyBloc, RickMortyState>(
        builder: (context, state) {
          if (state is RickMortyLoadingState) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }

          if (state is RickMortyExceptionState) {
            return const Center(
              child: Text("Произошла ошибка при получении данных"),
            );
          }

          if (state is RickMortySuccessState) {
            return ListView.builder(
              itemCount: state.queryCharacters.length,
              itemBuilder: (context, index) => PersonRowView(
                status: state.queryCharacters[index].status ?? "",
                fullName: state.queryCharacters[index].name ?? "",
                type: state.queryCharacters[index].species ?? "",
                gender: state.queryCharacters[index].gender ?? "",
                image: state.queryCharacters[index].image ?? "",
              ),
            );
          }

          return const SizedBox();
        },
      ),
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ListView.separated(
        itemBuilder: (context, index) {
          return Text(
            _suggestions[index],
            style: const TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
              color: Colors.white,
            ),
          );
        },
        separatorBuilder: (context, index) {
          return const Divider(
            thickness: 0.2,
          );
        },
        itemCount: _suggestions.length,
      ),
    );
  }
}

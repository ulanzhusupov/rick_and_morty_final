import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
// ignore: unnecessary_import
import 'package:provider/provider.dart';
import 'package:rick_and_morty_final/presentation/providers/rick_morty_provider.dart';
import 'package:rick_and_morty_final/presentation/theme/app_colors.dart';
import 'package:rick_and_morty_final/presentation/theme/app_fonts.dart';

class SearchTextField extends StatelessWidget {
  const SearchTextField({
    super.key,
    required this.hintText,
    required this.onFilterTap,
    required this.onChanged,
  });

  final String hintText;
  final Function(String query) onChanged;
  final Function() onFilterTap;

  @override
  Widget build(BuildContext context) {
    final theme = context.watch<RickMortyProvider>();
    return TextField(
      onChanged: (query) => onChanged(query),
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.symmetric(horizontal: 16),
        prefixIcon: const Icon(
          Icons.search,
          color: AppColors.grey,
        ),
        suffixIcon: IconButton(
          onPressed: onFilterTap,
          icon: const Icon(
            Icons.filter_alt,
            color: AppColors.grey,
          ),
        ),
        hintText: hintText,
        hintStyle: AppFonts.s16W400.copyWith(color: AppColors.grey),
        filled: true,
        fillColor: theme.isDarkTheme ? AppColors.inputBg : Colors.white,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(100),
        ),
      ),
    );
  }
}

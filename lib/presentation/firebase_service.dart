import "package:firebase_auth/firebase_auth.dart";

class FirebaseAuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future<User?> signUpWithEmailAndPassword(
      String email, String password) async {
    try {
      UserCredential credential = await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
      return credential.user;
    } catch (e) {
      if (e is FirebaseAuthException) {
        throw FirebaseAuthException(code: e.message?.split("/")[1] ?? "");
      }
    }
    return null;
  }

  Future<User?> signInWithEmailAndPassword(
      String email, String password) async {
    try {
      UserCredential credential = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      return credential.user;
    } catch (e) {
      if (e is FirebaseAuthException) {
        throw FirebaseAuthException(code: e.message ?? "");
      }
    }
    return null;
  }

  Future<void> logout() async {
    try {
      _auth.signOut();
    } catch (e) {
      if (e is FirebaseAuthException) {
        throw FirebaseAuthException(code: e.message?.split("/")[1] ?? "");
      }
    }
  }
}

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:rick_and_morty_final/domain/entities/episode_entity.dart';
import 'package:rick_and_morty_final/domain/entities/location_details_entity.dart';
import 'package:rick_and_morty_final/domain/entities/person_entity.dart';
import 'package:rick_and_morty_final/presentation/pages/auth/create_account_screen.dart';
import 'package:rick_and_morty_final/presentation/pages/auth/login_screen.dart';
import 'package:rick_and_morty_final/presentation/pages/characters/character_details_page.dart';
import 'package:rick_and_morty_final/presentation/pages/characters/filter_character_screen.dart';
import 'package:rick_and_morty_final/presentation/pages/episodes/episode_details_page.dart';
import 'package:rick_and_morty_final/presentation/pages/locations/location_details_screen.dart';
import 'package:rick_and_morty_final/presentation/pages/locations/location_filter_dimension_screen.dart';
import 'package:rick_and_morty_final/presentation/pages/locations/filter_location_screen.dart';
import 'package:rick_and_morty_final/presentation/pages/locations/location_type_screen.dart';
import 'package:rick_and_morty_final/presentation/pages/rick_morty_page.dart';
import 'package:rick_and_morty_final/presentation/pages/settings/change_name_page.dart';
import 'package:rick_and_morty_final/presentation/pages/settings/edit_profile_page.dart';
import 'package:rick_and_morty_final/presentation/pages/splash_screen.dart';
import 'package:rick_and_morty_final/presentation/router/auth_guard.dart';

part 'app_router.gr.dart';

@AutoRouterConfig()
class AppRouter extends _$AppRouter {
  @override
  List<AutoRoute> get routes => [
        AutoRoute(
          page: RickMortyRoute.page,
          initial: true,
          guards: [AuthGuard()],
        ),
        AutoRoute(
          page: CharacterDetailsRoute.page,
          guards: [AuthGuard()],
        ),
        AutoRoute(
          page: LocationDetailsRoute.page,
          guards: [AuthGuard()],
        ),
        AutoRoute(
          page: EpisodeDetailsRoute.page,
          guards: [AuthGuard()],
        ),
        AutoRoute(
          page: EditProfileRoute.page,
          guards: [AuthGuard()],
        ),
        AutoRoute(
          page: ChangeNameRoute.page,
          guards: [AuthGuard()],
          // initial: true,
        ),
        AutoRoute(
          page: LoginRoute.page,
          // initial: true,
        ),
        AutoRoute(
          page: CreateAccountRoute.page,
          // guards: [AuthGuard()],
          // initial: true,
        ),
        AutoRoute(
          page: SplashRoute.page,
          // initial: true,
        ),
        AutoRoute(
          page: FilterCharacterRoute.page,
          guards: [AuthGuard()],
          // initial: true,
        ),
        AutoRoute(
          page: FilterLocationRoute.page,
          guards: [AuthGuard()],
          // initial: true,
        ),
        AutoRoute(
          page: LocationFilterDimensionRoute.page,
          guards: [AuthGuard()],
          // initial: true,
        ),
        AutoRoute(
          page: LocationFilterTypeRoute.page,
          guards: [AuthGuard()],
          // initial: true,
        ),
      ];
}

import 'package:auto_route/auto_route.dart';
import 'package:rick_and_morty_final/presentation/router/app_router.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthGuard extends AutoRouteGuard {
  @override
  void onNavigation(NavigationResolver resolver, StackRouter router) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool loggedIn = prefs.getBool("logged_in") ?? false;
    if (loggedIn) {
      resolver.next(true);
    } else {
      router.push(LoginRoute(onResult: (result) {
        if (result == true) {
          resolver.next(true);
          router.removeLast();
        }
      }));
    }
  }
}

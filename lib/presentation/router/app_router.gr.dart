// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

part of 'app_router.dart';

abstract class _$AppRouter extends RootStackRouter {
  // ignore: unused_element
  _$AppRouter({super.navigatorKey});

  @override
  final Map<String, PageFactory> pagesMap = {
    ChangeNameRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const ChangeNameScreen(),
      );
    },
    CharacterDetailsRoute.name: (routeData) {
      final args = routeData.argsAs<CharacterDetailsRouteArgs>();
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: CharacterDetailsPage(
          key: args.key,
          character: args.character,
        ),
      );
    },
    CreateAccountRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const CreateAccountScreen(),
      );
    },
    EditProfileRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const EditProfileScreen(),
      );
    },
    EpisodeDetailsRoute.name: (routeData) {
      final args = routeData.argsAs<EpisodeDetailsRouteArgs>();
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: EpisodeDetailsPage(
          key: args.key,
          episode: args.episode,
        ),
      );
    },
    FilterCharacterRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const FilterCharacterScreen(),
      );
    },
    FilterLocationRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const FilterLocationScreen(),
      );
    },
    LocationDetailsRoute.name: (routeData) {
      final args = routeData.argsAs<LocationDetailsRouteArgs>();
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: LocationDetailsPage(
          key: args.key,
          location: args.location,
        ),
      );
    },
    LocationFilterDimensionRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const LocationFilterDimensionScreen(),
      );
    },
    LocationFilterTypeRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const LocationFilterTypeScreen(),
      );
    },
    LoginRoute.name: (routeData) {
      final args = routeData.argsAs<LoginRouteArgs>();
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: LoginScreen(
          key: args.key,
          onResult: args.onResult,
        ),
      );
    },
    RickMortyRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const RickMortyPage(),
      );
    },
    SplashRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const SplashScreen(),
      );
    },
  };
}

/// generated route for
/// [ChangeNameScreen]
class ChangeNameRoute extends PageRouteInfo<void> {
  const ChangeNameRoute({List<PageRouteInfo>? children})
      : super(
          ChangeNameRoute.name,
          initialChildren: children,
        );

  static const String name = 'ChangeNameRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [CharacterDetailsPage]
class CharacterDetailsRoute extends PageRouteInfo<CharacterDetailsRouteArgs> {
  CharacterDetailsRoute({
    Key? key,
    required PersonEntity character,
    List<PageRouteInfo>? children,
  }) : super(
          CharacterDetailsRoute.name,
          args: CharacterDetailsRouteArgs(
            key: key,
            character: character,
          ),
          initialChildren: children,
        );

  static const String name = 'CharacterDetailsRoute';

  static const PageInfo<CharacterDetailsRouteArgs> page =
      PageInfo<CharacterDetailsRouteArgs>(name);
}

class CharacterDetailsRouteArgs {
  const CharacterDetailsRouteArgs({
    this.key,
    required this.character,
  });

  final Key? key;

  final PersonEntity character;

  @override
  String toString() {
    return 'CharacterDetailsRouteArgs{key: $key, character: $character}';
  }
}

/// generated route for
/// [CreateAccountScreen]
class CreateAccountRoute extends PageRouteInfo<void> {
  const CreateAccountRoute({List<PageRouteInfo>? children})
      : super(
          CreateAccountRoute.name,
          initialChildren: children,
        );

  static const String name = 'CreateAccountRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [EditProfileScreen]
class EditProfileRoute extends PageRouteInfo<void> {
  const EditProfileRoute({List<PageRouteInfo>? children})
      : super(
          EditProfileRoute.name,
          initialChildren: children,
        );

  static const String name = 'EditProfileRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [EpisodeDetailsPage]
class EpisodeDetailsRoute extends PageRouteInfo<EpisodeDetailsRouteArgs> {
  EpisodeDetailsRoute({
    Key? key,
    required EpisodeEntity episode,
    List<PageRouteInfo>? children,
  }) : super(
          EpisodeDetailsRoute.name,
          args: EpisodeDetailsRouteArgs(
            key: key,
            episode: episode,
          ),
          initialChildren: children,
        );

  static const String name = 'EpisodeDetailsRoute';

  static const PageInfo<EpisodeDetailsRouteArgs> page =
      PageInfo<EpisodeDetailsRouteArgs>(name);
}

class EpisodeDetailsRouteArgs {
  const EpisodeDetailsRouteArgs({
    this.key,
    required this.episode,
  });

  final Key? key;

  final EpisodeEntity episode;

  @override
  String toString() {
    return 'EpisodeDetailsRouteArgs{key: $key, episode: $episode}';
  }
}

/// generated route for
/// [FilterCharacterScreen]
class FilterCharacterRoute extends PageRouteInfo<void> {
  const FilterCharacterRoute({List<PageRouteInfo>? children})
      : super(
          FilterCharacterRoute.name,
          initialChildren: children,
        );

  static const String name = 'FilterCharacterRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [FilterLocationScreen]
class FilterLocationRoute extends PageRouteInfo<void> {
  const FilterLocationRoute({List<PageRouteInfo>? children})
      : super(
          FilterLocationRoute.name,
          initialChildren: children,
        );

  static const String name = 'FilterLocationRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [LocationDetailsPage]
class LocationDetailsRoute extends PageRouteInfo<LocationDetailsRouteArgs> {
  LocationDetailsRoute({
    Key? key,
    required LocationDetailsEntity location,
    List<PageRouteInfo>? children,
  }) : super(
          LocationDetailsRoute.name,
          args: LocationDetailsRouteArgs(
            key: key,
            location: location,
          ),
          initialChildren: children,
        );

  static const String name = 'LocationDetailsRoute';

  static const PageInfo<LocationDetailsRouteArgs> page =
      PageInfo<LocationDetailsRouteArgs>(name);
}

class LocationDetailsRouteArgs {
  const LocationDetailsRouteArgs({
    this.key,
    required this.location,
  });

  final Key? key;

  final LocationDetailsEntity location;

  @override
  String toString() {
    return 'LocationDetailsRouteArgs{key: $key, location: $location}';
  }
}

/// generated route for
/// [LocationFilterDimensionScreen]
class LocationFilterDimensionRoute extends PageRouteInfo<void> {
  const LocationFilterDimensionRoute({List<PageRouteInfo>? children})
      : super(
          LocationFilterDimensionRoute.name,
          initialChildren: children,
        );

  static const String name = 'LocationFilterDimensionRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [LocationFilterTypeScreen]
class LocationFilterTypeRoute extends PageRouteInfo<void> {
  const LocationFilterTypeRoute({List<PageRouteInfo>? children})
      : super(
          LocationFilterTypeRoute.name,
          initialChildren: children,
        );

  static const String name = 'LocationFilterTypeRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [LoginScreen]
class LoginRoute extends PageRouteInfo<LoginRouteArgs> {
  LoginRoute({
    Key? key,
    required dynamic Function(bool) onResult,
    List<PageRouteInfo>? children,
  }) : super(
          LoginRoute.name,
          args: LoginRouteArgs(
            key: key,
            onResult: onResult,
          ),
          initialChildren: children,
        );

  static const String name = 'LoginRoute';

  static const PageInfo<LoginRouteArgs> page = PageInfo<LoginRouteArgs>(name);
}

class LoginRouteArgs {
  const LoginRouteArgs({
    this.key,
    required this.onResult,
  });

  final Key? key;

  final dynamic Function(bool) onResult;

  @override
  String toString() {
    return 'LoginRouteArgs{key: $key, onResult: $onResult}';
  }
}

/// generated route for
/// [RickMortyPage]
class RickMortyRoute extends PageRouteInfo<void> {
  const RickMortyRoute({List<PageRouteInfo>? children})
      : super(
          RickMortyRoute.name,
          initialChildren: children,
        );

  static const String name = 'RickMortyRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [SplashScreen]
class SplashRoute extends PageRouteInfo<void> {
  const SplashRoute({List<PageRouteInfo>? children})
      : super(
          SplashRoute.name,
          initialChildren: children,
        );

  static const String name = 'SplashRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:rick_and_morty_final/core/constant/app_icons.dart';
import 'package:rick_and_morty_final/presentation/pages/episodes/episodes_page.dart';
import 'package:rick_and_morty_final/presentation/pages/locations/locations_page.dart';
import 'package:rick_and_morty_final/presentation/pages/characters/persons_page.dart';
import 'package:rick_and_morty_final/presentation/pages/settings/settings_page.dart';
import 'package:rick_and_morty_final/presentation/theme/app_colors.dart';
import 'package:rick_and_morty_final/presentation/theme/app_fonts.dart';

@RoutePage()
class RickMortyPage extends StatefulWidget {
  const RickMortyPage({super.key});
  @override
  State<RickMortyPage> createState() => _RickMortyPageState();
}

class _RickMortyPageState extends State<RickMortyPage> {
  int _selectedIndex = 0;
  // static const TextStyle optionStyle =
  //     TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

  static const List<Widget> _widgetOptions = <Widget>[
    PersonsPage(),
    LocationsPage(),
    EpisodesPage(),
    SettingsPage(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.amber,
        selectedLabelStyle: AppFonts.s12W400.copyWith(color: AppColors.green),
        unselectedItemColor: AppColors.greyText,
        showUnselectedLabels: true,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              AppIcons.subtract,
              color: _selectedIndex == 0 ? AppColors.green : AppColors.greyText,
            ),
            label: 'Персонажи',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              AppIcons.location,
              color: _selectedIndex == 1 ? AppColors.green : AppColors.greyText,
            ),
            label: 'Локации',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              AppIcons.episodes,
              color: _selectedIndex == 2 ? AppColors.green : AppColors.greyText,
            ),
            label: 'Эпизоды',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              AppIcons.settings,
              color: _selectedIndex == 3 ? AppColors.green : AppColors.greyText,
            ),
            label: 'Настройки',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: AppColors.green,
        onTap: _onItemTapped,
      ),
    );
  }
}

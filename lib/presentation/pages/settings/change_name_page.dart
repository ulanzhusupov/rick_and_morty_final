import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:rick_and_morty_final/presentation/theme/app_colors.dart';
import 'package:rick_and_morty_final/presentation/theme/app_fonts.dart';
import 'package:rick_and_morty_final/presentation/widgets/custom_testfield.dart';

@RoutePage()
class ChangeNameScreen extends StatelessWidget {
  const ChangeNameScreen({super.key});

  @override
  Widget build(BuildContext context) {
    TextEditingController nameContr = TextEditingController();
    TextEditingController secondNameContr = TextEditingController();
    TextEditingController lastNameContr = TextEditingController();
    return Scaffold(
      appBar: AppBar(
        title: const Text("Редактировать профиль"),
        leading: IconButton(
          onPressed: () {
            context.router.pop();
          },
          icon: const Icon(
            Icons.arrow_back_ios_new_outlined,
            color: Colors.white,
            size: 16,
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: SizedBox(
          width: double.infinity,
          height: 48,
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              backgroundColor: AppColors.blueLight,
              shape: RoundedRectangleBorder(
                side: BorderSide.none,
                borderRadius: BorderRadius.circular(12),
              ),
            ),
            onPressed: () {},
            child: Text(
              "Сохранить",
              style: AppFonts.s16W400.copyWith(color: Colors.white),
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const SizedBox(height: 61),
            CustomTextField(
              label: "Имя",
              hintText: "Имя",
              controller: nameContr,
            ),
            CustomTextField(
              label: "Фамилия",
              hintText: "Фамилия",
              controller: secondNameContr,
            ),
            CustomTextField(
              label: "Отчество",
              hintText: "Отчество",
              controller: lastNameContr,
            ),
          ],
        ),
      ),
    );
  }
}

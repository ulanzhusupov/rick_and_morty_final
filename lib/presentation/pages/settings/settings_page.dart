import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:rick_and_morty_final/core/constant/app_icons.dart';
import 'package:rick_and_morty_final/core/constant/app_images.dart';
import 'package:rick_and_morty_final/presentation/bloc/cubit/auth_cubit.dart';
import 'package:rick_and_morty_final/presentation/providers/rick_morty_provider.dart';
import 'package:rick_and_morty_final/presentation/router/app_router.dart';
import 'package:rick_and_morty_final/presentation/theme/app_colors.dart';
import 'package:rick_and_morty_final/presentation/theme/app_fonts.dart';
import 'package:rick_and_morty_final/presentation/widgets/link_button.dart';

enum DarkTheme { dark, light }

class SettingsPage extends StatelessWidget {
  const SettingsPage({super.key});

  Future<void> _dialogBuilder(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        final vm = context.watch<RickMortyProvider>();
        return AlertDialog(
          title: const Text(
            'Темная тема',
          ),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              RadioListTile(
                title: Text(
                  "Выключена",
                  style: AppFonts.s16W400.copyWith(
                    color: vm.isDarkTheme ? Colors.white : AppColors.darkText,
                  ),
                ),
                value: "Выключена",
                groupValue: vm.selectedOption,
                onChanged: (val) => vm.changeSelectedOption(val ?? ""),
              ),
              RadioListTile(
                title: Text(
                  "Включена",
                  style: AppFonts.s16W400.copyWith(
                    color: vm.isDarkTheme ? Colors.white : AppColors.darkText,
                  ),
                ),
                value: "Включена",
                groupValue: vm.selectedOption,
                onChanged: (val) => vm.changeSelectedOption(val ?? ""),
              ),
              RadioListTile(
                title: Text(
                  "Следовать настройкам системы",
                  style: AppFonts.s16W400.copyWith(
                    color: vm.isDarkTheme ? Colors.white : AppColors.darkText,
                  ),
                ),
                value: "Следовать настройкам системы",
                groupValue: vm.selectedOption,
                onChanged: (val) => vm.changeSelectedOption(val ?? ""),
              ),
            ],
          ),
          actions: <Widget>[
            TextButton(
              style: TextButton.styleFrom(
                textStyle: Theme.of(context).textTheme.labelLarge,
              ),
              child: const Text('Disable'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              style: TextButton.styleFrom(
                textStyle: Theme.of(context).textTheme.labelLarge,
              ),
              child: const Text('Enable'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Настройки"),
      ),
      body: BlocListener<AuthCubit, AuthState>(
        listener: (context, state) {
          if (state is AuthSuccess) {
            print(state);
            context.router.push(const RickMortyRoute());
          }
        },
        child: SingleChildScrollView(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 33),
              Row(
                children: [
                  const CircleAvatar(
                    backgroundImage: AssetImage(AppImages.avatar),
                    radius: 40,
                  ),
                  const SizedBox(width: 16),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text("Oleg Belotserkovsky",
                          style: AppFonts.s16W400),
                      Text(
                        "Rick",
                        style: AppFonts.s14W400.copyWith(color: AppColors.grey),
                      ),
                    ],
                  ),
                ],
              ),
              const SizedBox(height: 30),
              SizedBox(
                width: double.infinity,
                height: 40,
                child: ElevatedButton(
                  onPressed: () {
                    context.router.push(const EditProfileRoute());
                  },
                  style: ElevatedButton.styleFrom(
                    elevation: 0,
                    backgroundColor: Colors.transparent,
                    shape: RoundedRectangleBorder(
                      side: const BorderSide(
                        color: AppColors.blueLight,
                      ),
                      borderRadius: BorderRadius.circular(12),
                    ),
                  ),
                  child: Text(
                    "Редактировать",
                    style:
                        AppFonts.s16W400.copyWith(color: AppColors.blueLight),
                  ),
                ),
              ),
              const SizedBox(height: 36),
              const Divider(color: AppColors.lightBg, height: 1),
              const SizedBox(height: 36),
              Text(
                "Внешний вид",
                style: AppFonts.s10W500.copyWith(color: AppColors.grey),
              ),
              const SizedBox(height: 24),
              LinkButton(
                  onTap: () {
                    _dialogBuilder(context);
                  },
                  icon: AppIcons.artist,
                  firstText: "Темная тема",
                  secondText: "Включена"),
              const SizedBox(height: 36),
              const Divider(color: AppColors.lightBg, height: 1),
              const SizedBox(height: 36),
              Text(
                "О приложении",
                style: AppFonts.s10W500.copyWith(color: AppColors.grey),
              ),
              const SizedBox(height: 24),
              const Text(
                "Зигерионцы помещают Джерри и Рика в симуляцию, чтобы узнать секрет изготовления концен-трирован- ной темной материи.",
                style: AppFonts.s13W400,
              ),
              const SizedBox(height: 36),
              const Divider(color: AppColors.lightBg, height: 1),
              const SizedBox(height: 36),
              Text(
                "Версия приложения",
                style: AppFonts.s10W500.copyWith(color: AppColors.grey),
              ),
              const SizedBox(height: 24),
              const Text(
                "Rick & Morty  v1.0.0",
                style: AppFonts.s13W400,
              ),
              const SizedBox(height: 24),
              Center(
                child: InkWell(
                  onTap: () {
                    BlocProvider.of<AuthCubit>(context).logout();
                  },
                  child: Text(
                    "Выйти",
                    style: AppFonts.s16W400.copyWith(color: AppColors.red),
                  ),
                ),
              ),
              const SizedBox(height: 60),
            ],
          ),
        ),
      ),
    );
  }
}

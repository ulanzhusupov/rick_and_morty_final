import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:rick_and_morty_final/core/constant/app_images.dart';
import 'package:rick_and_morty_final/presentation/router/app_router.dart';
import 'package:rick_and_morty_final/presentation/theme/app_colors.dart';
import 'package:rick_and_morty_final/presentation/theme/app_fonts.dart';
import 'package:rick_and_morty_final/presentation/widgets/link_button.dart';

@RoutePage()
class EditProfileScreen extends StatelessWidget {
  const EditProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Редактировать профиль"),
        leading: IconButton(
          onPressed: () {
            context.router.pop();
          },
          icon: const Icon(
            Icons.arrow_back_ios_new_outlined,
            color: Colors.white,
            size: 16,
          ),
        ),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 41),
            Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const CircleAvatar(
                    radius: 75,
                    backgroundImage: AssetImage(AppImages.avatar),
                  ),
                  const SizedBox(height: 20),
                  Text(
                    "Изменить фото",
                    style:
                        AppFonts.s16W400.copyWith(color: AppColors.blueLight),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 40),
            Text(
              "Профиль",
              style: AppFonts.s10W500.copyWith(color: AppColors.grey),
              textAlign: TextAlign.start,
            ),
            const SizedBox(height: 24),
            LinkButton(
              onTap: () => context.router.push(const ChangeNameRoute()),
              firstText: "Изменить ФИО",
              secondText: "Oleg Belotserkovsky",
            ),
            const SizedBox(height: 20),
            LinkButton(onTap: () {}, firstText: "Логин", secondText: "Rick"),
          ],
        ),
      ),
    );
  }
}

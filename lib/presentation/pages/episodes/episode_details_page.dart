import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rick_and_morty_final/core/constant/app_images.dart';
import 'package:rick_and_morty_final/domain/entities/episode_entity.dart';
import 'package:rick_and_morty_final/presentation/bloc/rick_morty_bloc.dart';
import 'package:rick_and_morty_final/presentation/router/app_router.dart';
import 'package:rick_and_morty_final/presentation/theme/app_colors.dart';
import 'package:rick_and_morty_final/presentation/theme/app_fonts.dart';
import 'package:rick_and_morty_final/presentation/widgets/person_row_view.dart';

@RoutePage()
class EpisodeDetailsPage extends StatefulWidget {
  const EpisodeDetailsPage({
    super.key,
    required this.episode,
  });
  final EpisodeEntity episode;

  @override
  State<EpisodeDetailsPage> createState() => _EpisodeDetailsPageState();
}

class _EpisodeDetailsPageState extends State<EpisodeDetailsPage> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<RickMortyBloc>(context)
        .add(GetSpecialCharacters(characters: widget.episode.characters));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 265,
              child: Stack(
                children: [
                  Container(
                    height: 218,
                    decoration: const BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage(
                          AppImages.episodeBg,
                        ),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  const Positioned(
                    bottom: 0, // Adjust the position as needed
                    left: 0,
                    right: 0,
                    child: Center(
                      child: CircleAvatar(
                        radius: 50,
                        backgroundColor: AppColors.blueLight,
                        child: Icon(
                          Icons.play_arrow_rounded,
                          size: 50,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 50,
                  ),
                  Text(
                    widget.episode.name,
                    style: AppFonts.s24W700,
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(
                    height: 55,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Премьера",
                        style: AppFonts.s12W400.copyWith(color: AppColors.grey),
                      ),
                      Text(widget.episode.airDate, style: AppFonts.s14W400),
                    ],
                  ),
                  const SizedBox(height: 36),
                  const Divider(
                    height: 2,
                    color: AppColors.lightBg,
                  ),
                  const SizedBox(height: 36),
                  const Text("Персонажи", style: AppFonts.s20W500),
                  const SizedBox(height: 12),
                  BlocBuilder<RickMortyBloc, RickMortyState>(
                    builder: (context, state) {
                      if (state is RickMortyLoadingState) {
                        return const Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                      if (state is RickMortyExceptionState) {
                        return Center(
                          child: Text(state.exception.toString()),
                        );
                      }

                      if (state is RickMortySuccessState) {
                        return Column(
                          children: state.characters.map((e) {
                            return InkWell(
                              onTap: () => context.router
                                  .push(CharacterDetailsRoute(character: e)),
                              child: PersonRowView(
                                status: e.status ?? "",
                                fullName: e.name ?? "",
                                type: e.species ?? "",
                                gender: e.gender ?? "",
                                image: e.image ?? "",
                              ),
                            );
                          }).toList(),
                        );
                      }

                      return const SizedBox();
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
// ignore: unnecessary_import
import 'package:provider/provider.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rick_and_morty_final/core/constant/app_images.dart';
import 'package:rick_and_morty_final/presentation/bloc/rick_morty_bloc.dart';
import 'package:rick_and_morty_final/presentation/providers/rick_morty_provider.dart';
import 'package:rick_and_morty_final/presentation/router/app_router.dart';
import 'package:rick_and_morty_final/presentation/theme/app_colors.dart';
import 'package:rick_and_morty_final/presentation/theme/app_fonts.dart';
import 'package:rick_and_morty_final/presentation/widgets/location_card.dart';
import 'package:rick_and_morty_final/presentation/widgets/search_textfield.dart';

class EpisodesPage extends StatefulWidget {
  const EpisodesPage({super.key});

  @override
  State<EpisodesPage> createState() => _EpisodesPageState();
}

class _EpisodesPageState extends State<EpisodesPage> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<RickMortyBloc>(context).add(GetCurrentEpisodesEvent());
  }

  @override
  Widget build(BuildContext context) {
    final vm = context.watch<RickMortyProvider>();
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 10),
              SearchTextField(
                hintText: "Найти эпизоды",
                onChanged: (query) => BlocProvider.of<RickMortyBloc>(context)
                    .add(SearchEpisodesEvent(query: query)),
                onFilterTap: () {},
              ),
              const SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Всего эпизодов: 200",
                    style: AppFonts.s10W500.copyWith(color: AppColors.grey),
                  )
                ],
              ),
              const SizedBox(height: 20),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.72,
                child: BlocBuilder<RickMortyBloc, RickMortyState>(
                  builder: (context, state) {
                    if (state is RickMortyLoadingState) {
                      return const Center(
                        child: CircularProgressIndicator(),
                      );
                    }

                    if (state is RickMortyExceptionState) {
                      return Center(
                        child: Column(
                          children: [
                            Image.asset(
                              AppImages.noEpisode,
                              width: 151,
                              height: 212,
                            ),
                            const SizedBox(height: 44),
                            Text(
                              "Эпизода с таким названием нет",
                              style: AppFonts.s16W400.copyWith(
                                color: AppColors.grey,
                              ),
                            )
                          ],
                        ),
                      );
                    }
                    if (state is RickMortySuccessState) {
                      return ListView.builder(
                        itemCount: state.episodes.length,
                        itemBuilder: (context, index) => InkWell(
                          onTap: () => context.router.push(
                            EpisodeDetailsRoute(
                              episode: state.episodes[index],
                            ),
                          ),
                          child: LocationCard(
                            name: state.episodes[index].name,
                            type: state.episodes[index].episode,
                            dimension: state.episodes[index].airDate,
                            isDark: vm.isDarkTheme,
                          ),
                        ),
                      );
                    }

                    return const SizedBox();
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

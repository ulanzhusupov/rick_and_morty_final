import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:rick_and_morty_final/core/constant/app_images.dart';
import 'package:rick_and_morty_final/presentation/bloc/cubit/auth_cubit.dart';
import 'package:rick_and_morty_final/presentation/providers/rick_morty_provider.dart';
import 'package:rick_and_morty_final/presentation/router/app_router.dart';
import 'package:rick_and_morty_final/presentation/theme/app_colors.dart';
import 'package:rick_and_morty_final/presentation/theme/app_fonts.dart';
import 'package:rick_and_morty_final/presentation/widgets/custom_testfield.dart';

@RoutePage()
class LoginScreen extends StatelessWidget {
  const LoginScreen({super.key, required this.onResult});
  final Function(bool) onResult;

  @override
  Widget build(BuildContext context) {
    TextEditingController loginController = TextEditingController();
    TextEditingController passwordController = TextEditingController();
    final theme = context.watch<RickMortyProvider>();
    return Scaffold(
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          children: [
            const SizedBox(height: 40),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(
                  onPressed: () {
                    if (theme.isDarkTheme) {
                      theme.changeSelectedOption("Выключен");
                    } else {
                      theme.changeSelectedOption("Включена");
                    }
                  },
                  icon: theme.isDarkTheme
                      ? const Icon(Icons.sunny)
                      : const Icon(Icons.night_shelter),
                ),
              ],
            ),
            Image.asset(
              AppImages.logo,
              width: 267,
            ),
            CustomTextField(
              label: "Email",
              hintText: "Email",
              keyboardType: TextInputType.emailAddress,
              controller: loginController,
              icon: const Icon(Icons.person, color: AppColors.grey),
            ),
            CustomTextField(
              label: "Пароль",
              hintText: "Пароль",
              keyboardType: TextInputType.visiblePassword,
              controller: passwordController,
              icon: const Icon(Icons.lock, color: AppColors.grey),
            ),
            const SizedBox(height: 24),
            Column(
              children: [
                SizedBox(
                  width: double.infinity,
                  height: 48,
                  child: ElevatedButton(
                    onPressed: () {
                      BlocProvider.of<AuthCubit>(context).signIn(
                          loginController.text, passwordController.text);
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: AppColors.blueLight,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12),
                      ),
                    ),
                    child: BlocBuilder<AuthCubit, AuthState>(
                      builder: (context, state) {
                        if (state is AuthLoading) {
                          return const SizedBox(
                            width: 24,
                            height: 24,
                            child: CircularProgressIndicator(),
                          );
                        }
                        return Text(
                          "Войти",
                          style: AppFonts.s16W400.copyWith(color: Colors.white),
                        );
                      },
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 24),
            BlocConsumer<AuthCubit, AuthState>(
              listener: (context, state) {
                if (state is AuthSuccess) {
                  context.router.push(const RickMortyRoute());
                }
              },
              builder: (context, state) {
                if (state is AuthError) {
                  return Text(
                    state.message,
                    style: AppFonts.s16W400.copyWith(color: AppColors.red),
                  );
                }
                return const SizedBox();
              },
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  "У вас еще нет аккаунта?",
                  style: AppFonts.s14W400,
                ),
                const SizedBox(width: 10),
                InkWell(
                  onTap: () {
                    context.router.push(const CreateAccountRoute());
                  },
                  child: Text(
                    "Создать ",
                    style: AppFonts.s14W400.copyWith(color: AppColors.green),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 60),
          ],
        ),
      ),
    );
  }
}

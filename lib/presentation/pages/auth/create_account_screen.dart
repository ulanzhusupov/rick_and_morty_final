import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:rick_and_morty_final/presentation/bloc/cubit/auth_cubit.dart';
import 'package:rick_and_morty_final/presentation/providers/rick_morty_provider.dart';
import 'package:rick_and_morty_final/presentation/theme/app_colors.dart';
import 'package:rick_and_morty_final/presentation/theme/app_fonts.dart';
import 'package:rick_and_morty_final/presentation/widgets/custom_testfield.dart';

@RoutePage()
class CreateAccountScreen extends StatelessWidget {
  const CreateAccountScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final theme = context.watch<RickMortyProvider>();
    TextEditingController nameContr = TextEditingController();
    TextEditingController secondNameContr = TextEditingController();
    TextEditingController lastNameContr = TextEditingController();
    TextEditingController loginController = TextEditingController();
    TextEditingController passwordController = TextEditingController();

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        leading: IconButton(
          onPressed: () {
            context.router.pop();
          },
          icon: Icon(
            Icons.arrow_back_ios_new_outlined,
            color: theme.isDarkTheme ? Colors.white : AppColors.darkText,
            size: 16,
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: SizedBox(
          width: double.infinity,
          height: 48,
          child: ElevatedButton(
            onPressed: () {
              BlocProvider.of<AuthCubit>(context)
                  .signUp(loginController.text, passwordController.text);
            },
            style: ElevatedButton.styleFrom(
              backgroundColor: AppColors.blueLight,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12),
              ),
            ),
            child: Text(
              "Создать",
              style: AppFonts.s16W400.copyWith(color: Colors.white),
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          children: [
            const Text("Создать аккаунт", style: AppFonts.s34W700),
            const SizedBox(height: 40),
            CustomTextField(
                label: "Имя", hintText: "Имя", controller: nameContr),
            CustomTextField(
                label: "Фамилия",
                hintText: "Фамилия",
                controller: secondNameContr),
            CustomTextField(
                label: "Отчество",
                hintText: "Отчество",
                controller: lastNameContr),
            const SizedBox(height: 36),
            Divider(
              color:
                  theme.isDarkTheme ? AppColors.grey : const Color(0xfff2f2f2),
              thickness: 1,
            ),
            const SizedBox(height: 36),
            CustomTextField(
              label: "Email",
              hintText: "Email",
              keyboardType: TextInputType.emailAddress,
              controller: loginController,
              icon: const Icon(Icons.person, color: AppColors.grey),
            ),
            CustomTextField(
              label: "Пароль",
              hintText: "Пароль",
              keyboardType: TextInputType.visiblePassword,
              controller: passwordController,
              icon: const Icon(Icons.lock, color: AppColors.grey),
            ),
            const SizedBox(height: 100),
          ],
        ),
      ),
    );
  }
}

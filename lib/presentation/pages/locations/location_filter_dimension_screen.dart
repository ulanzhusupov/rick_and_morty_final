import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:rick_and_morty_final/presentation/bloc/rick_morty_bloc.dart';
import 'package:rick_and_morty_final/presentation/providers/filter_location_provider.dart';
import 'package:rick_and_morty_final/presentation/providers/rick_morty_provider.dart';
import 'package:rick_and_morty_final/presentation/theme/app_colors.dart';
import 'package:rick_and_morty_final/presentation/theme/app_fonts.dart';

@RoutePage()
class LocationFilterDimensionScreen extends StatelessWidget {
  const LocationFilterDimensionScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final theme = context.watch<RickMortyProvider>();
    final filtLocation = context.watch<FilterLocationProvider>();
    List<String> dimensions = [
      "Dimension C-137",
      "Post-Apocalyptic Dimension",
      "Replacement Dimension",
      "Cronenberg Dimension",
      "Fantasy Dimension",
      "Dimension 5-126",
      "unknown",
    ];

    return Scaffold(
      appBar: AppBar(
        title: const Text("Выберите измерение"),
        leading: IconButton(
          onPressed: () {
            context.router.pop();
          },
          icon: Icon(
            Icons.arrow_back_ios_new_outlined,
            color: theme.isDarkTheme ? Colors.white : AppColors.darkText,
            size: 16,
          ),
        ),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 36),
            Text(filtLocation.dimension, style: AppFonts.s16W400),
            const SizedBox(height: 24),
            const Divider(color: AppColors.lightBg, height: 1),
            const SizedBox(height: 12),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.5,
              child: ListView.builder(
                itemCount: dimensions.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.symmetric(vertical: 12),
                    child: InkWell(
                      onTap: () {
                        filtLocation.changeDimension(dimensions[index]);
                        BlocProvider.of<RickMortyBloc>(context).add(
                          FilterLocationEvent(
                            dimension: filtLocation.dimension != "Не выбрано"
                                ? filtLocation.dimension
                                : '',
                            type: filtLocation.locationType != "Не выбрано"
                                ? filtLocation.locationType
                                : '',
                          ),
                        );
                        context.router.pop();
                      },
                      child: Text(
                        dimensions[index],
                        style: AppFonts.s16W400.copyWith(
                          color: filtLocation.dimension == dimensions[index]
                              ? AppColors.green
                              : Colors.white,
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rick_and_morty_final/domain/entities/location_details_entity.dart';
import 'package:rick_and_morty_final/presentation/bloc/rick_morty_bloc.dart';
import 'package:rick_and_morty_final/presentation/router/app_router.dart';
import 'package:rick_and_morty_final/presentation/theme/app_colors.dart';
import 'package:rick_and_morty_final/presentation/theme/app_fonts.dart';
import 'package:rick_and_morty_final/presentation/widgets/person_row_view.dart';

@RoutePage()
class LocationDetailsPage extends StatefulWidget {
  const LocationDetailsPage({super.key, required this.location});
  final LocationDetailsEntity location;

  @override
  State<LocationDetailsPage> createState() => _LocationDetailsPageState();
}

class _LocationDetailsPageState extends State<LocationDetailsPage> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<RickMortyBloc>(context)
        .add(GetSpecialEpisodes(episodes: widget.location.residents));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 30),
            Text(widget.location.name, style: AppFonts.s24W700),
            Text(
              "${widget.location.type} · ${widget.location.dimension}",
              style: AppFonts.s12W400.copyWith(color: AppColors.grey),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(height: 36),
                const Text("Персонажи", style: AppFonts.s20W500),
                const SizedBox(height: 12),
                BlocBuilder<RickMortyBloc, RickMortyState>(
                  builder: (context, state) {
                    if (state is RickMortyLoadingState) {
                      return const Center(
                        child: CircularProgressIndicator(),
                      );
                    }
                    if (state is RickMortyExceptionState) {
                      return Center(
                        child: Text(state.exception.toString()),
                      );
                    }

                    if (state is RickMortySuccessState) {
                      return SizedBox(
                        height: MediaQuery.of(context).size.height * 0.8,
                        child: ListView.builder(
                          itemCount: state.characters.length,
                          itemBuilder: (context, index) => InkWell(
                            onTap: () => context.router.push(
                                CharacterDetailsRoute(
                                    character: state.characters[index])),
                            child: PersonRowView(
                              status: state.characters[index].status ?? "",
                              fullName: state.characters[index].name ?? "",
                              type: state.characters[index].species ?? "",
                              gender: state.characters[index].gender ?? "",
                              image: state.characters[index].image ?? "",
                            ),
                          ),
                        ),
                      );
                    }

                    return const SizedBox();
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

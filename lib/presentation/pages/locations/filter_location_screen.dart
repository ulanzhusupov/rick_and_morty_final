import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:rick_and_morty_final/core/constant/app_icons.dart';
import 'package:rick_and_morty_final/presentation/providers/filter_location_provider.dart';
import 'package:rick_and_morty_final/presentation/providers/rick_morty_provider.dart';
import 'package:rick_and_morty_final/presentation/router/app_router.dart';
import 'package:rick_and_morty_final/presentation/theme/app_colors.dart';
import 'package:rick_and_morty_final/presentation/theme/app_fonts.dart';
import 'package:rick_and_morty_final/presentation/widgets/link_button.dart';

@RoutePage()
class FilterLocationScreen extends StatelessWidget {
  const FilterLocationScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final vm = context.watch<RickMortyProvider>();
    final filterLocation = context.watch<FilterLocationProvider>();
    return Scaffold(
      appBar: AppBar(
        title: const Text("Фильтр локаций"),
        leading: IconButton(
          onPressed: () {
            context.router.pop();
          },
          icon: Icon(
            Icons.arrow_back_ios_new_outlined,
            color: vm.isDarkTheme ? Colors.white : AppColors.darkText,
            size: 16,
          ),
        ),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 24),
            Text(
              "Сортировать",
              style: AppFonts.s10W500.copyWith(color: AppColors.grey),
            ),
            const SizedBox(height: 29),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text("По алфавиту ", style: AppFonts.s16W400),
                Row(
                  children: [
                    InkWell(
                      onTap: () {},
                      child: SvgPicture.asset(AppIcons.ascending),
                    ),
                    const SizedBox(
                      width: 24,
                    ),
                    InkWell(
                      onTap: () {},
                      child: SvgPicture.asset(AppIcons.descending),
                    ),
                  ],
                ),
              ],
            ),
            const SizedBox(height: 36),
            const Divider(color: AppColors.lightBg, height: 1),
            const SizedBox(height: 36),
            Text(
              "Фильтровать по:",
              style: AppFonts.s10W500.copyWith(color: AppColors.grey),
            ),
            const SizedBox(height: 36),
            LinkButton(
              firstText: "Тип",
              secondText: filterLocation.locationType != "Не выбрано"
                  ? filterLocation.locationType
                  : "Выберите тип локации",
              onTap: () => context.router.push(const LocationFilterTypeRoute()),
            ),
            const SizedBox(height: 36),
            LinkButton(
              firstText: "Измерение",
              secondText: filterLocation.dimension != "Не выбрано"
                  ? filterLocation.dimension
                  : "Выберите измерения локации",
              onTap: () =>
                  context.router.push(const LocationFilterDimensionRoute()),
            ),
          ],
        ),
      ),
    );
  }
}

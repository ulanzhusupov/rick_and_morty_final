import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
// ignore: unnecessary_import
import 'package:provider/provider.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rick_and_morty_final/core/constant/app_images.dart';
import 'package:rick_and_morty_final/presentation/bloc/rick_morty_bloc.dart';
import 'package:rick_and_morty_final/presentation/providers/rick_morty_provider.dart';
import 'package:rick_and_morty_final/presentation/router/app_router.dart';
import 'package:rick_and_morty_final/presentation/theme/app_colors.dart';
import 'package:rick_and_morty_final/presentation/theme/app_fonts.dart';
import 'package:rick_and_morty_final/presentation/widgets/location_card.dart';
import 'package:rick_and_morty_final/presentation/widgets/search_textfield.dart';

class LocationsPage extends StatefulWidget {
  const LocationsPage({super.key});

  @override
  State<LocationsPage> createState() => _LocationsPageState();
}

class _LocationsPageState extends State<LocationsPage> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<RickMortyBloc>(context).add(GetCurrentLocationsEvent());
  }

  @override
  Widget build(BuildContext context) {
    final vm = context.watch<RickMortyProvider>();
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 10),
              SearchTextField(
                hintText: "Найти локацию",
                onChanged: (query) => BlocProvider.of<RickMortyBloc>(context)
                    .add(SearchLocationEvent(query: query)),
                onFilterTap: () =>
                    context.router.push(const FilterLocationRoute()),
              ),
              const SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Всего локаций: 200",
                    style: AppFonts.s10W500.copyWith(color: AppColors.grey),
                  )
                ],
              ),
              const SizedBox(height: 20),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.72,
                child: BlocBuilder<RickMortyBloc, RickMortyState>(
                  builder: (context, state) {
                    if (state is RickMortyLoadingState) {
                      return const Center(
                        child: CircularProgressIndicator(),
                      );
                    }

                    if (state is RickMortyExceptionState) {
                      return Center(
                        child: Column(
                          children: [
                            Image.asset(
                              AppImages.noLocation,
                              height: 135,
                            ),
                            const SizedBox(height: 44),
                            Text(
                              "Локации с таким названием нет",
                              style: AppFonts.s16W400.copyWith(
                                color: AppColors.grey,
                              ),
                            )
                          ],
                        ),
                      );
                    }
                    if (state is RickMortySuccessState) {
                      return ListView.builder(
                        itemCount: state.locations.length,
                        itemBuilder: (context, index) => InkWell(
                          onTap: () => context.router.push(
                            LocationDetailsRoute(
                                location: state.locations[index]),
                          ),
                          child: LocationCard(
                            name: state.locations[index].name,
                            type: state.locations[index].type,
                            dimension: state.locations[index].dimension,
                            isDark: vm.isDarkTheme,
                          ),
                        ),
                      );
                    }

                    return const SizedBox();
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

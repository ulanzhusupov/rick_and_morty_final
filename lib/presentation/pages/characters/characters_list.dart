import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
// ignore: unnecessary_import
import 'package:provider/provider.dart';
import 'package:rick_and_morty_final/core/constant/app_images.dart';
import 'package:rick_and_morty_final/domain/entities/person_entity.dart';
import 'package:rick_and_morty_final/presentation/bloc/rick_morty_bloc.dart';
import 'package:rick_and_morty_final/presentation/providers/filter_character_provider.dart';
import 'package:rick_and_morty_final/presentation/router/app_router.dart';
import 'package:rick_and_morty_final/presentation/theme/app_colors.dart';
import 'package:rick_and_morty_final/presentation/theme/app_fonts.dart';
import 'package:rick_and_morty_final/presentation/widgets/person_grid_view.dart';
import 'package:rick_and_morty_final/presentation/widgets/person_row_view.dart';

class CharactersList extends StatelessWidget {
  const CharactersList({
    super.key,
    required this.isListView,
  });

  final bool isListView;

  @override
  Widget build(BuildContext context) {
    final filterCharacter = context.watch<FilterCharacterProvider>();
    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.72,
      child: BlocBuilder<RickMortyBloc, RickMortyState>(
        builder: (context, state) {
          if (state is RickMortyLoadingState) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }

          if (state is RickMortyExceptionState) {
            return Center(
              child: Column(
                children: [
                  Image.asset(
                    AppImages.noCharacter,
                    width: 150,
                    height: 251,
                  ),
                  const SizedBox(height: 44),
                  Text(
                    "Эпизода с таким названием нет",
                    style: AppFonts.s16W400.copyWith(
                      color: AppColors.grey,
                    ),
                  )
                ],
              ),
            );
          }

          if (state is RickMortySuccessState) {
            List<PersonEntity> allCharacters =
                state.characters.where((element) {
              if (filterCharacter.isAlive && filterCharacter.isDead) {
                return element.status == "Alive" || element.status == "Dead";
              }
              if (filterCharacter.isAlive && filterCharacter.isUnknown) {
                return element.status == "Alive" || element.status == "unknown";
              }
              if (filterCharacter.isDead && filterCharacter.isUnknown) {
                return element.status == "Dead" || element.status == "unknown";
              }
              if (filterCharacter.isAlive &&
                  filterCharacter.isDead &&
                  filterCharacter.isUnknown) {
                return element.status == "Alive" ||
                    element.status == "Dead" ||
                    element.status == "unknown";
              }
              if (filterCharacter.isAlive) {
                return element.status == "Alive";
              }
              if (filterCharacter.isDead) {
                return element.status == "Dead";
              }
              if (filterCharacter.isUnknown) {
                return element.status == "unknown";
              }
              return true;
            }).toList();

            if (isListView) {
              return ListView.builder(
                itemCount: allCharacters.length,
                itemBuilder: (context, index) => InkWell(
                  onTap: () => context.router.push(CharacterDetailsRoute(
                      character: state.characters[index])),
                  child: PersonRowView(
                    status: allCharacters[index].status ?? "",
                    fullName: allCharacters[index].name ?? "",
                    type: allCharacters[index].species ?? "",
                    gender: allCharacters[index].gender ?? "",
                    image: allCharacters[index].image ?? "",
                  ),
                ),
              );
            } else {
              return GridView.builder(
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                ),
                shrinkWrap: true,
                itemCount: state.characters.length,
                itemBuilder: (context, index) => InkWell(
                  onTap: () => context.router.push(CharacterDetailsRoute(
                    character: state.characters[index],
                  )),
                  child: PersonGridView(
                    status: state.characters[index].status ?? "",
                    fullName: state.characters[index].name ?? "",
                    type: state.characters[index].species ?? "",
                    gender: state.characters[index].gender ?? "",
                    image: state.characters[index].image ?? "",
                  ),
                ),
              );
            }
          }

          return const SizedBox();
        },
      ),
    );
  }
}

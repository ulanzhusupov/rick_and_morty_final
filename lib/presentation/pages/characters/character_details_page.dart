import 'dart:ui';

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rick_and_morty_final/domain/entities/person_entity.dart';
import 'package:rick_and_morty_final/presentation/bloc/rick_morty_bloc.dart';
import 'package:rick_and_morty_final/presentation/theme/app_colors.dart';
import 'package:rick_and_morty_final/presentation/theme/app_fonts.dart';
import 'package:rick_and_morty_final/presentation/widgets/character_info_text.dart';
import 'package:rick_and_morty_final/presentation/widgets/episode_link.dart';

@RoutePage()
class CharacterDetailsPage extends StatefulWidget {
  const CharacterDetailsPage({super.key, required this.character});
  final PersonEntity character;

  @override
  State<CharacterDetailsPage> createState() => _CharacterDetailsPageState();
}

class _CharacterDetailsPageState extends State<CharacterDetailsPage> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<RickMortyBloc>(context)
        .add(GetSpecialEpisodes(episodes: widget.character.episodes ?? []));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 298,
              child: Stack(
                children: [
                  Container(
                    height: 218,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: NetworkImage(
                          widget.character.image ?? "",
                        ),
                        fit: BoxFit.cover,
                      ),
                    ),
                    child: ClipRect(
                      child: BackdropFilter(
                        filter: ImageFilter.blur(sigmaX: 5.0, sigmaY: 3),
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.white.withOpacity(0.0)),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 0, // Adjust the position as needed
                    left: 0,
                    right: 0,
                    child: Center(
                      child: CircleAvatar(
                        radius: 81,
                        backgroundColor: AppColors.bg,
                        child: CircleAvatar(
                          radius: 73,
                          backgroundColor: AppColors.bg,
                          backgroundImage: NetworkImage(
                            widget.character.image ?? "",
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 24),
            Text(widget.character.name ?? "", style: AppFonts.s24W700),
            Text(
              widget.character.status ?? "",
              style: AppFonts.s10W500.copyWith(color: AppColors.green),
            ),
            const SizedBox(height: 30),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                children: [
                  Row(
                    children: [
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.5,
                        child: CharacterInfoText(
                          title: "Пол",
                          content: widget.character.gender ?? "",
                        ),
                      ),
                      CharacterInfoText(
                        title: "Расса",
                        content: widget.character.species ?? "",
                      ),
                    ],
                  ),
                  const SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      CharacterInfoText(
                        title: "Место рождения",
                        content: widget.character.origin?.name ?? "",
                      ),
                      RotatedBox(
                        quarterTurns: 10,
                        child: IconButton(
                          onPressed: () {},
                          icon: const Icon(
                            Icons.arrow_back_ios_new_outlined,
                            color: Colors.white,
                            size: 16,
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 24),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      CharacterInfoText(
                        title: "Местоположение",
                        content: widget.character.location?.name ?? "",
                      ),
                      RotatedBox(
                        quarterTurns: 10,
                        child: IconButton(
                          onPressed: () {},
                          icon: const Icon(
                            Icons.arrow_back_ios_new_outlined,
                            color: Colors.white,
                            size: 16,
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 36),
                  const Divider(color: AppColors.lightBg, height: 2),
                  const SizedBox(height: 36),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text("Эпизоды", style: AppFonts.s20W500),
                      Text("Все эпизоды",
                          style:
                              AppFonts.s12W400.copyWith(color: AppColors.grey)),
                    ],
                  ),
                  const SizedBox(height: 27),
                  BlocBuilder<RickMortyBloc, RickMortyState>(
                    builder: (context, state) {
                      if (state is RickMortyLoadingState) {
                        return const Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                      if (state is RickMortyExceptionState) {
                        return Center(
                          child: Text(state.exception.toString()),
                        );
                      }

                      if (state is RickMortySuccessState) {
                        return Column(
                          children: state.episodes
                              .map((e) => EpisodeLink(
                                    episode: e.episode,
                                    title: e.name,
                                    date: e.airDate,
                                  ))
                              .toList(),
                        );
                      }

                      return const SizedBox();
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

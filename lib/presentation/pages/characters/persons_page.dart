import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rick_and_morty_final/presentation/bloc/rick_morty_bloc.dart';
import 'package:rick_and_morty_final/presentation/pages/characters/characters_list.dart';
import 'package:rick_and_morty_final/presentation/router/app_router.dart';
import 'package:rick_and_morty_final/presentation/theme/app_colors.dart';
import 'package:rick_and_morty_final/presentation/theme/app_fonts.dart';
import 'package:rick_and_morty_final/presentation/widgets/search_textfield.dart';

class PersonsPage extends StatefulWidget {
  const PersonsPage({super.key});

  @override
  State<PersonsPage> createState() => _PersonsPageState();
}

class _PersonsPageState extends State<PersonsPage> {
  bool isListView = true;

  @override
  void initState() {
    super.initState();
    BlocProvider.of<RickMortyBloc>(context).add(GetCurrentCharacterEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            children: [
              const SizedBox(height: 10),
              SearchTextField(
                hintText: "Найти персонажа",
                onChanged: (query) => BlocProvider.of<RickMortyBloc>(context)
                    .add(SearchCharacterEvent(query: query)),
                onFilterTap: () {
                  context.router.push(const FilterCharacterRoute());
                },
              ),
              const SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Всего персонажей: ",
                    style: AppFonts.s10W500.copyWith(color: AppColors.grey),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        isListView = !isListView;
                      });
                    },
                    child: Icon(
                      isListView ? Icons.grid_view_rounded : Icons.list_rounded,
                      color: AppColors.grey,
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 20),
              CharactersList(isListView: isListView),
            ],
          ),
        ),
      ),
    );
  }
}

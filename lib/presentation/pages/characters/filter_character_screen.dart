import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:rick_and_morty_final/core/constant/app_icons.dart';
import 'package:rick_and_morty_final/presentation/providers/filter_character_provider.dart';
import 'package:rick_and_morty_final/presentation/providers/rick_morty_provider.dart';
import 'package:rick_and_morty_final/presentation/theme/app_colors.dart';
import 'package:rick_and_morty_final/presentation/theme/app_fonts.dart';
import 'package:rick_and_morty_final/presentation/widgets/custom_checkbox.dart';

@RoutePage()
class FilterCharacterScreen extends StatelessWidget {
  const FilterCharacterScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final theme = context.watch<RickMortyProvider>();
    final vm = context.watch<FilterCharacterProvider>();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.lightBg,
        title: const Text("Фильтры"),
        leading: IconButton(
          onPressed: () {
            context.router.pop();
          },
          icon: Icon(
            Icons.arrow_back_ios_new_outlined,
            color: theme.isDarkTheme ? Colors.white : AppColors.darkText,
            size: 16,
          ),
        ),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 24),
            Text(
              "Сортировать",
              style: AppFonts.s10W500.copyWith(color: AppColors.grey),
            ),
            const SizedBox(height: 29),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text("По алфавиту ", style: AppFonts.s16W400),
                Row(
                  children: [
                    InkWell(
                      onTap: () {},
                      child: SvgPicture.asset(AppIcons.ascending),
                    ),
                    const SizedBox(
                      width: 24,
                    ),
                    InkWell(
                      onTap: () {},
                      child: SvgPicture.asset(AppIcons.descending),
                    ),
                  ],
                ),
              ],
            ),
            const SizedBox(height: 36),
            const Divider(color: AppColors.lightBg, height: 1),
            const SizedBox(height: 36),
            Text(
              "Статус",
              style: AppFonts.s10W500.copyWith(color: AppColors.grey),
            ),
            const SizedBox(height: 24),
            CustomCheckBox(
              label: "Живой",
              value: vm.isAlive,
              onTap: (bool? val) => vm.changeIsAlive(),
            ),
            CustomCheckBox(
              label: "Мертвый",
              value: vm.isDead,
              onTap: (bool? val) => vm.changeIsDead(),
            ),
            CustomCheckBox(
              label: "Неизвестно",
              value: vm.isUnknown,
              onTap: (bool? val) => vm.changeIsUnknown(),
            ),
            const SizedBox(height: 36),
            const Divider(color: AppColors.lightBg, height: 1),
            const SizedBox(height: 36),
            Text(
              "Пол",
              style: AppFonts.s10W500.copyWith(color: AppColors.grey),
            ),
            const SizedBox(height: 24),
            CustomCheckBox(
              label: "Мужской",
              value: vm.isMale,
              onTap: (bool? val) => vm.changeIsMale(),
            ),
            CustomCheckBox(
              label: "Женский",
              value: vm.isFemale,
              onTap: (bool? val) => vm.changeIsFemale(),
            ),
            CustomCheckBox(
              label: "Бесполый",
              value: vm.isNoGender,
              onTap: (bool? val) => vm.changeIsNoGender(),
            ),
          ],
        ),
      ),
    );
  }
}

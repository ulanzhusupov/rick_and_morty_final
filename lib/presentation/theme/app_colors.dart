import 'package:flutter/material.dart';

final class AppColors {
  static const Color bg = Color(0xff0B1E2D);
  static const Color green = Color(0xff43D049);
  static const Color red = Color(0xffEB5757);
  static const Color blue = Color(0xff22a2bdde);
  static const Color blueLight = Color(0xff22A2BD);
  static const Color lightBg = Color(0xff152A3A);
  static const Color greyText = Color(0xffA3A3A3);
  static const Color grey = Color(0xff5B6975);
  static const Color inputBg = Color(0xff152A3A);
  static const Color darkText = Color(0xff0B1E2D);
}

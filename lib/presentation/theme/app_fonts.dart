import 'package:flutter/material.dart';

final class AppFonts {
  static const TextStyle s10W500 = TextStyle(
    fontSize: 10,
    fontWeight: FontWeight.w500,
  );

  static const TextStyle s12W400 = TextStyle(
    fontSize: 12,
    fontWeight: FontWeight.w400,
  );

  static const TextStyle s13W400 = TextStyle(
    fontSize: 13,
    fontWeight: FontWeight.w400,
  );

  static const TextStyle s14W400 = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w400,
  );

  static const TextStyle s14W500 = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w500,
  );

  static const TextStyle s16W400 = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w400,
  );

  static const TextStyle s16W500 = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w500,
  );

  static const TextStyle s34W400 = TextStyle(
    fontSize: 34,
    fontWeight: FontWeight.w400,
  );
  static const TextStyle s34W700 = TextStyle(
    fontSize: 34,
    fontWeight: FontWeight.w700,
  );

  static const TextStyle s20W500 = TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.w500,
  );

  static const TextStyle s24W700 = TextStyle(
    fontSize: 24,
    fontWeight: FontWeight.w700,
  );
}

import 'package:flutter/material.dart';

class FilterCharacterProvider extends ChangeNotifier {
  bool isAlive = false;
  bool isDead = false;
  bool isUnknown = false;
  bool isMale = false;
  bool isFemale = false;
  bool isNoGender = false;
  int charactersCount = 0;

  void setCharactersCount(int count) {
    charactersCount = count;
    notifyListeners();
  }

  void changeIsAlive() {
    isAlive = !isAlive;
    notifyListeners();
  }

  void changeIsDead() {
    isDead = !isDead;
    notifyListeners();
  }

  void changeIsUnknown() {
    isUnknown = !isUnknown;
    notifyListeners();
  }

  void changeIsMale() {
    isMale = !isMale;
    notifyListeners();
  }

  void changeIsFemale() {
    isFemale = !isFemale;
    notifyListeners();
  }

  void changeIsNoGender() {
    isNoGender = !isNoGender;
    notifyListeners();
  }
}

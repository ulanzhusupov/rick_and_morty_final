import 'package:flutter/material.dart';

class FilterLocationProvider extends ChangeNotifier {
  String dimension = "Не выбрано";
  String locationType = "Не выбрано";

  void changeLocationType(String type) {
    if (locationType == type) {
      locationType = "Не выбрано";
    } else {
      locationType = type;
    }
    notifyListeners();
  }

  void changeDimension(String dimensn) {
    if (dimension == dimensn) {
      dimension = "Не выбрано";
    } else {
      dimension = dimensn;
    }
    notifyListeners();
  }
}

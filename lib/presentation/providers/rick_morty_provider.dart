import 'package:flutter/material.dart';

class RickMortyProvider extends ChangeNotifier {
  bool isDarkTheme = true;
  String selectedOption = "Включена";

  void changeSelectedOption(String val) {
    selectedOption = val;
    if (selectedOption == "Включена") {
      isDarkTheme = true;
    } else {
      isDarkTheme = false;
    }
    notifyListeners();
  }
}

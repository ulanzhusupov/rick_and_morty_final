import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:rick_and_morty_final/core/constant/app_consts.dart';
import 'package:rick_and_morty_final/presentation/firebase_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  AuthCubit({required this.authService, required this.preferences})
      : super(AuthInitial());
  final FirebaseAuthService authService;
  final SharedPreferences preferences;

  void signIn(String email, String password) async {
    try {
      emit(AuthLoading());
      User? user =
          await authService.signInWithEmailAndPassword(email, password);
      if (user != null) {
        preferences.setBool(AppConsts.loggedIn, true);
        emit(AuthSuccess(user: user));
      }
    } catch (e) {
      emit(AuthError(message: e.toString()));
    }
  }

  void signUp(String email, String password) async {
    try {
      emit(AuthLoading());
      User? user =
          await authService.signUpWithEmailAndPassword(email, password);
      if (user != null) {
        preferences.setBool(AppConsts.loggedIn, true);
        emit(AuthSuccess(user: user));
      }
    } catch (e) {
      emit(AuthError(message: e.toString()));
    }
  }

  void logout() async {
    try {
      emit(AuthLoading());
      authService.logout();
      preferences.setBool(AppConsts.loggedIn, false);
      emit(const AuthSuccess(user: null));
    } catch (e) {
      emit(AuthError(message: e.toString()));
    }
  }
}

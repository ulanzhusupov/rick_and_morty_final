part of 'rick_morty_bloc.dart';

abstract class RickMortyState extends Equatable {
  const RickMortyState();

  @override
  List<Object> get props => [];
}

class RickMortyInitialState extends RickMortyState {}

class RickMortyLoadingState extends RickMortyState {}

class RickMortySuccessState extends RickMortyState {
  const RickMortySuccessState({
    required this.characters,
    required this.queryCharacters,
    required this.episodes,
    required this.locations,
  });

  final List<PersonEntity> characters;
  final List<PersonEntity> queryCharacters;
  final List<EpisodeEntity> episodes;
  final List<LocationDetailsEntity> locations;

  @override
  List<Object> get props => [characters, queryCharacters, episodes, locations];
}

class RickMortyExceptionState extends RickMortyState {
  const RickMortyExceptionState(this.exception);
  final dynamic exception;

  @override
  List<Object> get props => [exception];
}

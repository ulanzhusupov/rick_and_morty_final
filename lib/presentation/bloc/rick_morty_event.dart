part of 'rick_morty_bloc.dart';

abstract class RickMortyEvent extends Equatable {
  const RickMortyEvent();

  @override
  List<Object> get props => [];
}

class GetCurrentCharacterEvent extends RickMortyEvent {}

class GetMoreCharactersEvent extends RickMortyEvent {}

class SearchCharacterEvent extends RickMortyEvent {
  const SearchCharacterEvent({required this.query});

  final String query;

  @override
  List<Object> get props => [query];
}

class GetCurrentEpisodesEvent extends RickMortyEvent {}

class GetSpecialCharacters extends RickMortyEvent {
  const GetSpecialCharacters({required this.characters});

  final List<String> characters;

  @override
  List<Object> get props => [characters];
}

class SearchEpisodesEvent extends RickMortyEvent {
  const SearchEpisodesEvent({required this.query});

  final String query;

  @override
  List<Object> get props => [query];
}

class GetSpecialEpisodes extends RickMortyEvent {
  const GetSpecialEpisodes({required this.episodes});

  final List<String> episodes;

  @override
  List<Object> get props => [episodes];
}

class GetCurrentLocationsEvent extends RickMortyEvent {}

class SearchLocationEvent extends RickMortyEvent {
  const SearchLocationEvent({required this.query});

  final String query;

  @override
  List<Object> get props => [query];
}

class FilterLocationEvent extends RickMortyEvent {
  const FilterLocationEvent({
    required this.type,
    required this.dimension,
  });

  final String type;
  final String dimension;

  @override
  List<Object> get props => [type, dimension];
}

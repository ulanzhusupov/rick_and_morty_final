import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rick_and_morty_final/domain/entities/episode_entity.dart';
import 'package:rick_and_morty_final/domain/entities/location_details_entity.dart';
import 'package:rick_and_morty_final/domain/entities/person_entity.dart';
import 'package:rick_and_morty_final/domain/usecase/rick_morty_usecase.dart';

part 'rick_morty_event.dart';
part 'rick_morty_state.dart';

class RickMortyBloc extends Bloc<RickMortyEvent, RickMortyState> {
  RickMortyBloc({required this.rickMortyUseCase})
      : super(RickMortyInitialState()) {
    on<GetCurrentCharacterEvent>(_getCurrentCharacter);
    on<GetMoreCharactersEvent>(_getMoreCharacters);
    on<SearchCharacterEvent>(_searchCharacters);
    on<GetSpecialCharacters>(_getSpecialCharacters);
    on<GetCurrentEpisodesEvent>(_getCurrentEpisodes);
    on<GetSpecialEpisodes>(_getSpecialEpisodes);
    on<SearchEpisodesEvent>(_searchEpisodes);
    on<GetCurrentLocationsEvent>(_getCurrentLocations);
    on<SearchLocationEvent>(_searchLocations);
    on<FilterLocationEvent>(_filterLocation);
  }

  final RickMortyUseCase rickMortyUseCase;
  int page = 1;
  int locationsPage = 1;
  int episodesPage = 1;

  Future<void> _getCurrentCharacter(
    GetCurrentCharacterEvent event,
    Emitter<RickMortyState> emit,
  ) async {
    try {
      emit(RickMortyLoadingState());
      await rickMortyUseCase.getAllCharacters(page);

      emit(globalState());
    } catch (e) {
      emit(RickMortyExceptionState(e));
    }
  }

  Future<void> _getMoreCharacters(
    GetMoreCharactersEvent event,
    Emitter<RickMortyState> emit,
  ) async {
    try {
      page++;
      emit(RickMortyLoadingState());
      await rickMortyUseCase.getAllCharacters(page);

      emit(globalState());
    } catch (e) {
      emit(RickMortyExceptionState(e));
    }
  }

  Future<void> _searchCharacters(
    SearchCharacterEvent event,
    Emitter<RickMortyState> emit,
  ) async {
    try {
      emit(RickMortyLoadingState());
      await rickMortyUseCase.searchCharacter(event.query);

      emit(globalState());
    } catch (e) {
      emit(RickMortyExceptionState(e));
    }
  }

  Future<void> _getSpecialCharacters(
    GetSpecialCharacters event,
    Emitter<RickMortyState> emit,
  ) async {
    try {
      emit(RickMortyLoadingState());
      await rickMortyUseCase.getSpecialCharacters(event.characters);

      emit(globalState());
    } catch (e) {
      emit(RickMortyExceptionState(e));
    }
  }

  Future<void> _searchEpisodes(
    SearchEpisodesEvent event,
    Emitter<RickMortyState> emit,
  ) async {
    try {
      emit(RickMortyLoadingState());
      await rickMortyUseCase.searchEpisodes(event.query);

      emit(globalState());
    } catch (e) {
      emit(RickMortyExceptionState(e));
    }
  }

  Future<void> _getCurrentEpisodes(
    GetCurrentEpisodesEvent event,
    Emitter<RickMortyState> emit,
  ) async {
    try {
      emit(RickMortyLoadingState());
      await rickMortyUseCase.getAllEpisodes(episodesPage);

      emit(globalState());
    } catch (e) {
      emit(RickMortyExceptionState(e));
    }
  }

  Future<void> _getSpecialEpisodes(
    GetSpecialEpisodes event,
    Emitter<RickMortyState> emit,
  ) async {
    try {
      emit(RickMortyLoadingState());
      await rickMortyUseCase.getSpecialEpisodes(event.episodes);

      emit(globalState());
    } catch (e) {
      emit(RickMortyExceptionState(e));
    }
  }

  Future<void> _getCurrentLocations(
    GetCurrentLocationsEvent event,
    Emitter<RickMortyState> emit,
  ) async {
    try {
      emit(RickMortyLoadingState());
      await rickMortyUseCase.getAllLocations(locationsPage);

      emit(globalState());
    } catch (e) {
      emit(RickMortyExceptionState(e));
    }
  }

  Future<void> _searchLocations(
    SearchLocationEvent event,
    Emitter<RickMortyState> emit,
  ) async {
    try {
      emit(RickMortyLoadingState());
      await rickMortyUseCase.searchLocation(event.query);

      emit(globalState());
    } catch (e) {
      emit(RickMortyExceptionState(e));
    }
  }

  Future<void> _filterLocation(
    FilterLocationEvent event,
    Emitter<RickMortyState> emit,
  ) async {
    try {
      emit(RickMortyLoadingState());
      await rickMortyUseCase.filterLocation(event.type, event.dimension);

      emit(globalState());
    } catch (e) {
      emit(RickMortyExceptionState(e));
    }
  }

  RickMortySuccessState globalState() => RickMortySuccessState(
        characters: rickMortyUseCase.characters ?? [],
        queryCharacters: rickMortyUseCase.queryCharacters ?? [],
        episodes: rickMortyUseCase.episodes ?? [],
        locations: rickMortyUseCase.locations ?? [],
      );
}
